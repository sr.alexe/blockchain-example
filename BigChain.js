const driver = require('bigchaindb-driver')

const Alex = new driver.Ed25519Keypair()
const conn = new driver.Connection('https://test.bigchaindb.com/api/v1/')
const tx = driver.Transaction.makeCreateTransaction(
    { message: 'Texto1' },
    null,
    [ driver.Transaction.makeOutput(
        driver.Transaction.makeEd25519Condition(Alex.publicKey))],
    Alex.publicKey)
const txSigned = driver.Transaction.signTransaction(tx, Alex.privateKey)
conn.postTransactionCommit(txSigned)
const tx2 = driver.Transaction.makeCreateTransaction(
    { message: 'Texto2' },
    null,
    [ driver.Transaction.makeOutput(
        driver.Transaction.makeEd25519Condition(Alex.publicKey))],
    Alex.publicKey)
const txSigned2 = driver.Transaction.signTransaction(tx2, Alex.privateKey)
conn.postTransactionCommit(txSigned2)
console.log(Alex.publicKey);
//https://test.bigchaindb.com/api/v1/outputs?public_key=
//https://test.bigchaindb.com/api/v1/transactions/ 
